# Welcome to Framer

# This is all CoffeeScript. Learn here: http://framerjs.com/learn.html#coffeescript

# Drop an image on the preview screen to create an image layer, or use the generator to import assets from Sketch or Photoshop


# Define a set of states with names (the original state is 'default')

imageLayer = new Layer 
	x:0, y:0, width:320, height:400, image:"images/request_backup.png"
	
requestButton = new Layer x:43, y:90, width:232, height:230, backgroundColor:"transparent"

receiveRequest = new Layer x:43, y:330, width:232, height:70, backgroundColor:"transparent"

imageLayer.addSubLayer(requestButton)
imageLayer.addSubLayer(receiveRequest)
 
requestButton.on Events.Click, ->
	imageLayer.visible = false
	imageLayer4.visible = true
	
receiveRequest.on Events.Click, -> 
	imageLayer.visible = false
	imageLayer5.visible = true
	

imageLayer4 = new Layer 
	x:0, y:0, width:320, height:410, image:"images/select_severity.png", visible:false
	
imageLayer4.on Events.Click, ->
	imageLayer4.visible = false
	imageLayer1.visible = true
	


# imageLayer1 = new Layer 
# 	x:0, y:0, width:322, height:402, image:"images/derive-watch-no-drifts.png"
# imageLayer1.on Events.Click, ->
# 	imageLayer2.visible = true
# 	imageLayer2.bringToFront()
# 
# 
# imageLayer2 = new Layer x:0, y:0, width:322, height:402, image:"images/derive-watch-invitation.png", visible:false
# 
# decline = new Layer x:47, y:243, width:75, height:75, backgroundColor:"transparent"
# accept = new Layer x:160, y:243, width:75, height:75, backgroundColor:"transparent"
# 
# imageLayer2.addSubLayer(decline)
# imageLayer2.addSubLayer(accept) 
# 
# accept.on Events.Click, ->
# 	imageLayer3.visible = true
# 	imageLayer3.bringToFront()
# 	
# decline.on Events.Click, ->
# 	imageLayer1.bringToFront()
# 
#  
# imageLayer3 = new Layer x:0, y:0, width:320, height:400, image:"images/derive-watch-waiting.png", visible:false
# imageLayer3.on Events.Click, ->
# 	imageLayer4.visible = true
# 	imageLayer4.bringToFront()
# 
#  
# imageLayer4 = new Layer x:0, y:0, width:320, height:400, image:"images/derive-watch-1.png", visible:false
# imageLayer4.on Events.Click, ->
# 	imageLayer5.visible = true
# 	imageLayer5.bringToFront()
# 
# 
# imageLayer5 = new Layer x:0, y:0, width:320, height:400, image:"images/derive-watch-2.png", visible:false
# imageLayer5.on Events.Click, ->
# 	imageLayer6.visible = true
# 	imageLayer6.bringToFront()
# 
# 
# imageLayer6 = new Layer x:0, y:0, width:320, height:400, image:"images/derive-watch-3.png", visible:false
# imageLayer6.on Events.Click, ->
# 	imageLayer7.visible = true
# 	imageLayer7.bringToFront()
# 
# 
# imageLayer7 = new Layer x:0, y:0, width:320, height:400, image:"images/derive-watch-4.png", visible:false
# imageLayer7.on Events.Click, -> 
# 	imageLayer8.visible = true
# 	imageLayer8.bringToFront() 
#  
# imageLayer8 = new Layer x:0, y:0, width:320, height:400, image:"images/derive-watch-5.png", visible:false
# imageLayer8.on Events.Click, ->
# 	imageLayer9.visible = true
# 	imageLayer9.bringToFront() 
# 
# imageLayer9 = new Layer x:0, y:0, width:320, height:400, image:"images/derive-watch-6.png", visible:false
# imageLayer9.on Events.Click, ->
# 	imageLayer10.visible = true
# 	imageLayer10.bringToFront() 
# 
#  
# imageLayer10 = new Layer x:0, y:0, width:320, height:400, image:"images/derive-watch-7.png", visible:false




imageLayer1 = new Layer 
	x:0, y:0, width:320, height:410, image:"images/request_sent.png", visible:false
	
cancelButton = new Layer x:11, y:336, width:145, height:75, backgroundColor:"transparent"
resolveButton = new Layer x:164, y:336, width:145, height:75, backgroundColor:"transparent"

imageLayer1.addSubLayer(cancelButton)
imageLayer1.addSubLayer(resolveButton) 

resolveButton.on Events.Click, ->
	imageLayer1.visible = false
	imageLayer2.visible = true
	
cancelButton.on Events.Click, ->
	imageLayer1.visible = false
	imageLayer.visible = true

imageLayer2 = new Layer 
	x:0, y:0, width:320, height:410, image:"images/categorize_incident.png", visible:false
	
imageLayer2.on Events.Click, ->
	imageLayer2.visible = false
	imageLayer3.visible = true
		
imageLayer3 = new Layer 
	x:0, y:0, width:320, height:410, image:"images/incident_resolved.png", visible:false
	
resolvedOkButton = new Layer x:55, y:240, width:210, height:58, backgroundColor:"transparent"

imageLayer3.addSubLayer(resolvedOkButton)

resolvedOkButton.on Events.Click, ->
	imageLayer3.visible = false
	imageLayer.visible = true


imageLayer5 = new Layer 
	x:0, y:0, width:320, height:410, image:"images/request_received.png", visible: false

declineRequestButton = new Layer x:11, y:336, width:145, height:75, backgroundColor:"transparent"
acceptRequestButton = new Layer x:164, y:336, width:145, height:75, backgroundColor:"transparent"

imageLayer5.addSubLayer(declineRequestButton)
imageLayer5.addSubLayer(acceptRequestButton) 

acceptRequestButton.on Events.Click, ->
	imageLayer5.visible = false
	imageLayer6.visible = true
	
declineRequestButton.on Events.Click, ->
	imageLayer5.visible = false
	imageLayer.visible = true

	

imageLayer6 = new Layer 
	x:0, y:0, width:320, height:410, image:"images/request_accepted.png", visible:false
	
imageLayer6.on Events.Click, ->
	imageLayer6.visible = false
	imageLayer.visible = true
