# Welcome to Framer

# Learn how to prototype: http://framerjs.com/learn
# Drop an image on the device, or import a design from Sketch or Photoshop


# Define a set of states with names (the original state is 'default')
# Set the default animation options
# On a click, go to the next state

imageLayer = new Layer 
	x:0, y:0, width:1080, height:1920, image:"images/event_list_no_scrollbar.png"
imageLayer.on Events.Click, ->
	imageLayer.visible = false
	imageLayer1.visible = true
 
imageLayer1 = new Layer 
	x:0, y:0, width:1080, height:1920, image:"images/cal_vs_oregon_no_scrollbar.png", visible:false
	
calOregonBackButton = new Layer x:25, y:40, width:100, height: 100, backgroundColor:"transparent"

imageLayer1.addSubLayer(calOregonBackButton)

calOregonBackButton.on Events.Click, ->
	imageLayer1.visible = false
	imageLayer.visible = true
	
incidentsButton = new Layer x:62, y:1115, width:960, height: 150, backgroundColor:"transparent"

imageLayer1.addSubLayer(incidentsButton)

incidentsButton.on Events.Click, ->
	imageLayer1.visible = false
	imageLayer2.visible = true
	

imageLayer2 = new Layer 
	x:0, y:0, width:1080, height:1920, image:"images/incident_list_no_scrollbar.png", visible:false
	
incidentsBackButton = new Layer x:25, y:40, width:100, height: 100, backgroundColor:"transparent"

imageLayer2.addSubLayer(incidentsBackButton)

incidentsBackButton.on Events.Click, ->
	imageLayer2.visible = false
	imageLayer1.visible = true
	
requestsResponsesButton = new Layer x:62, y:300, width:960, height: 195, backgroundColor:"transparent"

requestsResponsesButton.on Events.Click, ->
	imageLayer1.visible = false
	imageLayer3.visible = true 
 
incidentsByLocationButton = new Layer x:62, y:505, width:960, height: 195, backgroundColor:"transparent"

incidentsByLocationButton.on Events.Click, ->
	imageLayer1.visible = false
	imageLayer4.visible = true 

incidentsOverTimeButton = new Layer x:62, y:710, width:960, height: 195, backgroundColor:"transparent"

incidentsOverTimeButton.on Events.Click, ->
	imageLayer1.visible = false 
	imageLayer5.visible = true 
	
imageLayer1.addSubLayer(requestsResponsesButton)  
imageLayer1.addSubLayer(incidentsByLocationButton)
imageLayer1.addSubLayer(incidentsOverTimeButton)
	 
imageLayer3 = new Layer   
	x:0, y:0, width:1080, height:1980, image:"images/requests_and_responses.png", visible:false
	
imageLayer4 = new Layer  
	x:0, y:0, width:1080, height:1980, image:"images/incidents_by_location.png", visible:false 
	
imageLayer5 = new Layer 
	x:0, y:0, width:1080, height:1980, image:"images/incidents_over_time.png", visible:false
	
requestsResponsesBackButton = new Layer x:25, y:40, width:100, height: 100, backgroundColor:"transparent"

imageLayer3.addSubLayer(requestsResponsesBackButton)

requestsResponsesBackButton.on Events.Click, ->
	imageLayer3.visible = false
	imageLayer1.visible = true
	
incidentsByLocationBackButton = new Layer x:25, y:40, width:100, height: 100, backgroundColor:"transparent"

imageLayer4.addSubLayer(incidentsByLocationBackButton)

incidentsByLocationBackButton.on Events.Click, ->
	imageLayer4.visible = false
	imageLayer1.visible = true
	
incidentsOverTimeBackButton = new Layer x:25, y:40, width:100, height: 100, backgroundColor:"transparent"

imageLayer5.addSubLayer(incidentsOverTimeBackButton)

incidentsOverTimeBackButton.on Events.Click, ->
	imageLayer5.visible = false
	imageLayer1.visible = true 
	
sectionC4Button = new Layer x:62, y:300, width:960, height: 195, backgroundColor:"transparent"

imageLayer2.addSubLayer(sectionC4Button)

sectionC4Button.on Events.Click, ->
	imageLayer2.visible = false
	imageLayer6.visible = true  
	
imageLayer6 = new Layer 
	x:0, y:0, width:1080, height:1920, image:"images/describe_incident_step1.png", visible:false
	
secionC4BackButton = new Layer x:25, y:40, width:100, height: 100, backgroundColor:"transparent"

imageLayer6.addSubLayer(secionC4BackButton)

secionC4BackButton.on Events.Click, ->
	imageLayer6.visible = false
	imageLayer2.visible = true
	
imageLayer7 = new Layer 
	x:0, y:0, width:1080, height:1920, image:"images/describe_incident_step2.png", visible:false
	
descriptionButton = new Layer x:62, y:650, width:960, height: 195, backgroundColor:"transparent"

imageLayer6.addSubLayer(descriptionButton)

descriptionButton.on Events.Click, ->
	imageLayer6.visible = false 
	imageLayer7.visible = true 
	
secionC42BackButton = new Layer x:25, y:40, width:100, height: 100, backgroundColor:"transparent"

imageLayer7.addSubLayer(secionC42BackButton)

secionC42BackButton.on Events.Click, ->
	imageLayer7.visible = false
	imageLayer2.visible = true
	
secionC4SaveButton = new Layer x:950, y:40, width:100, height: 100, backgroundColor:"transparent"

imageLayer6.addSubLayer(secionC4SaveButton)

secionC4SaveButton.on Events.Click, ->
	imageLayer6.visible = false
	imageLayer2.visible = true
	
secionC42SaveButton = new Layer x:950, y:40, width:100, height: 100, backgroundColor:"transparent"

imageLayer7.addSubLayer(secionC42SaveButton)

secionC42SaveButton.on Events.Click, ->
	imageLayer7.visible = false
	imageLayer2.visible = true
